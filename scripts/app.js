document.addEventListener('DOMContentLoaded', function() {
const list = [];

const form = document.querySelector('.shopping-form');
const ItemInput = document.querySelector('.shopping-form-item-input');
const quantityInput = document.querySelector('.shopping-form-quantity-input');
const incrementButton = document.querySelector('.shopping-form-increment-button');
const decrementButton = document.querySelector('.shopping-form-decrement-button');

incrementButton.addEventListener('click', incrementQuantity)
decrementButton.addEventListener('click', decrementQuantity);

form.addEventListener('submit', addItemToList);

function incrementQuantity() {
    const currentValue = Number(quantityInput.value);
    const newValue = currentValue +1;

    quantityInput.value = newValue;
}

function decrementQuantity(){
    const currentValue = Number(quantityInput.value)
    const newValue = currentValue - 1;

    if(newValue > 0 ) {
        quantityInput.value = newValue
    }
}

function addItemToList(event) {
    event.preventDefault();

    const ItemName = event.target;

    console.log(event.target)["item-name"].value;

    console.log(ItemName)
    


}

});